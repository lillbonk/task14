//TASK 14 - Multiply 2 values
function multiply(num1, num2) {
   return num1 * num2;  
 }
const multi = multiply(5,3);


//TASK 14 - Add 3 values
function adds(num1, num2, num3) {
    return num1 + num2 + num3;  
  }
 const multiAdd = adds(5,3,5);


//TASK 14 - Divide a value in half
 function divide(num1) {
    return num1 /2;
  }
 const multiDivide = divide(6);



 //TASK 14 - Multiply the sum of 2 values by the third value
 function multiplySum(num1, num2, num3) {
    return ((num1 + num2) * num3);  
  }
 const multiSum = multiplySum(5,5,10);




//  //declare a variable in global
//  let var1 = "1"

//   if (true) {
//       // since this variable is inside a block it is only available in here.
//       let var2 = "2"
//   }

//   // If I try to access it here, it cannot be found
// console.log(var2);
// // but var1 is available ;)
// console.log(var1);



//let and const are not hoisted, but var is. Which means that var can be reached outside of a function/block
var var3 = "A"

  if (true) {
      //if this was a let or const it could not be found outside of block, but when made into a var it can be found.
      var var4 = "B"
  }
  
  // here both varables can ve reached
    console.log(var3);
    console.log(var4);
  


    //Coercion

const add = 1 + "1";                // will be value 11 since string is selected first
const add2 = 1+1;                   // will be value 2, since numbers are just added togheter
const add3 = "Hello" + " Dewald";   // Will just add the the 2 string values together
const add4 = Boolean('');           // false, since noothing in can be converted to 0/false
const add5 = Boolean(0);            // false. 0 is converted to false
const add6 = Boolean(-0);           // false. Since nothing is converted to false
const add7 = Boolean(NaN);          // false. Since nothing is converted to false
const add8 = Boolean(null);         // false. Since nothing is converted to false




function Employee(employeenumber, name, surname, email) {
    this.employeeNumber = employeenumber,
    this.name = name,
    this.surname = surname,
    this.email = email
       this.getFullname = function(){
        return name + " " + surname;
       };
       this.contactCard = function(){
        return "ID: "+ employeenumber + " Name: " + name + " " + surname;
       };
       };

const bruce = new Employee(666, 'Bruce', 'Wayne', "Bruce@Brucewayne.com");
const robin = new Employee(007, 'Robin', 'NoLastName', "Robin@Brucewayne.com");




const bestProjectEver = {
    name: "JavaScript Project for Dummies",
    description: "JavaScript for realllly dumb people",
    startdate: "2020-03-03",
    enddate: "To be done before the end of humanity",
    budget: "1 billion dollarz",
    projectManager: "Albert Einstein",
    participans: "10-25",
    active: true, 
};